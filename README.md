# Messages Consumer

## About

A messages consumer, listening into the Fedora Messaging bus matching the schemas that it is configured for and comparing the messages against the awarding conditions available on the Collection, and on a successful match, making a request to the Accolades API for awarding the said badge to a certain contributor who has met the conditions specified in the related badge rule

## Read more

1. https://fedora-arc.readthedocs.io/en/latest/badges/prop_rewrite_entities.html#internal-entities
